import sys
from datetime import datetime

import requests
import json

import Influx
import constants

url = constants.url
token = constants.dt_token
get_service_url = constants.get_service_url
get_process_url = constants.get_process_url
get_host_url = constants.get_host_url


def get_metrics(value):
    if not value:
        raise Exception("Command line arguments needs be provided")
    host_ip_args = value
    print("Application started at: ", datetime.now())
    # Request to get running services

    my_response = requests.get(url, headers={'Authorization': token}, verify=True)
    if my_response.ok:
        j_data = json.loads(my_response.content)
    else:
        my_response.raise_for_status()

    timestamp = ""
    service = []
    rps_list = []
    rt_list = []
    data1 = j_data["result"]

    # Parse service response Json
    for d in data1:
        metric_type = d["metricId"]
        for d1 in d["data"]:
            # Extract service timestamp from response
            timestamp = d1["timestamps"]
            if check_metric_id(metric_type, "requestCount"):
                # Extract service RPS from response
                rps = d1["values"]
                rps_list.append(rps)
            elif check_metric_id(metric_type, "response.time"):
                # Extract service response time from response
                rt = d1["values"]
                rt_list.append(rt)
            for d2 in d1["dimensions"]:
                # Extract service id from response
                service.append(d2)

    # Convert Unix timestamp to UTC timestamp for Influx
    time_list = convert_utc_time(timestamp)

    host_ip_list = {}
    host_ip_list = set()
    for rps, rt, serv in zip(rps_list, rt_list, service):
        service_name = get_service_name(serv)[0]
        host_name = get_service_name(serv)[1]
        host_ip_list.add(host_name)
        if host_name in host_ip_args:
            Influx.insert_metrics(rps, rt, service_name, time_list, host_name)
        else:
            print("Ip not matches with args: ", host_name)

    print("Collected last 2 hour Dynatrace metrics for hosts: ", host_ip_list)
    print("Application ended at: ", datetime.now())


# Check data for response time and RPS
def check_metric_id(string, sub_str):
    if string.find(sub_str) == -1: return False
    return True


# get service name to store in InfluxDB
def get_service_name(s_id):
    service_temp = []
    host_temp = []
    s_url = get_service_url + s_id
    my_response = requests.get(s_url, headers={'Authorization': token}, verify=True)
    if my_response.ok:
        j_data = json.loads(my_response.content)
        if isinstance(j_data, dict):
            j_data_array = [j_data]
        else:
            j_data_array = j_data
        for data in j_data_array:
            service_temp.append(data["displayName"])
            from_relationship = data["fromRelationships"]
            runs_on = from_relationship["runsOnProcessGroupInstance"]
            for process_id in runs_on:
                host_temp = get_host_id(process_id)
    else:
        my_response.raise_for_status()
    return service_temp, host_temp


# Get host id to get hostname
def get_host_id(process_id):
    p_url = get_process_url + process_id
    my_response = requests.get(p_url, headers={'Authorization': token}, verify=True)
    if my_response.ok:
        j_data = json.loads(my_response.content)
        from_relationship = j_data["fromRelationships"]
        is_process_of = from_relationship["isProcessOf"]
        for host_id in is_process_of:
            return get_host_ip(host_id)
    else:
        my_response.raise_for_status()


# Get host public ip to store in InfluxDB
def get_host_ip(host_id):
    h_url = get_host_url + host_id
    my_response = requests.get(h_url, headers={'Authorization': token}, verify=True)
    if my_response.ok:
        j_data = json.loads(my_response.content)
        host_name = j_data["publicIp"]
        return host_name
    else:
        my_response.raise_for_status()


def convert_utc_time(timestamp):
    utc_time = []
    for posix_time in timestamp:
        utc_time.append(datetime.fromtimestamp(posix_time / 1000).strftime('%Y-%m-%dT%H:%M:%S'))
    return utc_time


if __name__ == '__main__':
    get_metrics(sys.argv[1:])
