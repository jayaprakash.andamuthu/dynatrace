from datetime import datetime

from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS

import constants


def insert_metrics(rps, rt, service, timestamp, host):
    token = constants.influx_token
    org = constants.org
    bucket = constants.bucket

    # print("rps: ", rps, "rt: ", rt, "service: ", service, "host: ", host)

    client = InfluxDBClient(url="http://localhost:8086", token=token)
    write_api = client.write_api(write_options=SYNCHRONOUS)
    rps_temp = rps
    rt_temp = rt
    timestamp_temp = timestamp
    service_name = service
    host_name = host
    for request_count, response_time, utc_time in zip(rps_temp, rt_temp, timestamp_temp):
        if request_count is not None:
            p = Point("metrics").field("RPS", float(request_count)).field("RT", float(response_time / 1000)).tag(
                "Host", host_name).tag("Service", service_name).time(utc_time)
            try:
                write_api.write(bucket, org, p)
            except Exception as e:
                print("Insertion in InfluxDB failed: ", e)
                exit()

